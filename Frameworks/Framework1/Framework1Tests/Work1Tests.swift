//
//  Work1Tests.swift
//  Framework1Tests
//
//  Created by qiang xu on 2018/10/20.
//  Copyright © 2018 qiang xu. All rights reserved.
//

import XCTest

@testable import Framework1

class Work1Tests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        let result = work1()
        XCTAssertEqual(result, "work1")
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
